FROM ubuntu:18.04

RUN apt-get update && apt-get install -y ca-certificates vim wget

RUN echo "deb [trusted=yes] https://deb.natade-coco.com/ ./\n" >> /etc/apt/sources.list.d/natade-coco.list
RUN apt-get update && apt-get install -y printbridge
RUN systemctl enable printbridge

RUN mkdir -p /var/run/dbus
ENV init /lib/systemd/systemd

ENTRYPOINT ["/sbin/init"]
