# printbridge for edge

Please use this when connect an edge unit runing a [photo print app](https://gitlab.com/natade-coco/apps/photo-print) with a photo printer. 

## 🚧 Release Candidate

This is _pre-release_ software. Changes may occur at any time, and
certain features might be missing or broken. 

## ⚙️ Install

```bash
$ kubectl apply -f printbridge/manifests
```

## 🚀 Usage

Get a printer endpoint
```
$ kubectl exec -it printbridge-XXXXX -- /bin/bash
# driverless
ipp://CP13005cb1b3.local:631/ipp/print #=> YOUR_PRINTER_ENDPOINT
```

Edit configmap
```
$ vi manifests/configmap.yaml
[Bridge]
Printer = YOUR_PRINTER_ENDPOINT
...
[Directus]
Endpoint = YOUR_UNIT_DATASTORE_ENDPOINT
Email = YOUR_UNIT_DATASTORE_LOGIN_EMAIL
Password = YOUR_UNIT_DATASTORE_LOGIN_PASS
...
```

Apply updated configmap
```
$ kubectl apply -f printbridge/manifests
```

